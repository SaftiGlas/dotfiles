#!/bin/bash
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'


#Light Theme
echo "${cyn}Welcome back to the ${mag}90s!"

#Wallpaper
xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitor0/workspace0/last-image --set /home/saft/Pictures/Wallpaper/windows95.jpg
#Theme
xfconf-query --channel xsettings --property /Net/ThemeName --set Chicago95
xfconf-query --channel xfwm4 --property /general/theme --set Chicago95
#icon
xfconf-query --channel xsettings --property /Net/IconThemeName --set Chicago95
#font
#xfconf-query --channel xsettings --property /Gtk/FontName --set Sans --set 10
#xfconf-query --channel xfwn4 --property /general/title_font --set Sans Bold 9

#PAnel
xfconf-query --channel xfce4-panel --property /panels/panel-1/size --set 29
xfconf-query --channel xfce4-panel --property /plugins/plugin-3/show-labels --set true

