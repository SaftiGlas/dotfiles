#!/bin/bash
red=$'\e[1;31m'
grn=$'\e[1;32m'
yel=$'\e[1;33m'
blu=$'\e[1;34m'
mag=$'\e[1;35m'
cyn=$'\e[1;36m'
end=$'\e[0m'

#Dark Theme
echo "Switching to ${red}MacOS xD"
#Wallpaper
xfconf-query --channel xfce4-desktop --property /backdrop/screen0/monitor0/workspace0/last-image --set /home/saft/Pictures/Wallpaper/macos_sierra_2-wallpaper-2560x1600.jpg
#Theme
xfconf-query --channel xsettings --property /Net/ThemeName --set Arc-Dark
xfconf-query --channel xfwm4 --property /general/theme --set Arc-Dark
#icon
xfconf-query --channel xsettings --property /Net/IconThemeName --set Paper
#fonts
#xfconf-query --channel xsettings --property /Gtk/FontName --set SFNS Display 11
#xfconf-query --channel xfwn4 --propserty /general/title_font --set SFNS Display Bold 10

#Panel
xfconf-query --channel xfce4-panel --property /panels/panel-1/size --set 26
xfconf-query --channel xfce4-panel --property /plugins/plugin-3/show-labels --set false
#xfconf-query --channel xfce4-panel --property /panels/panel-1/position --set p=6;x=683;y=13
